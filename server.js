var express = require('express');
var bodyParser = require('body-parser');
var userFile = require('./user.json');
var totalUsers = 0;
var app = express();
app.use(bodyParser.json());

const URI_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

//Metodo para obtener el total de usuarios o para ejecutar el Query String
app.get(URI_BASE + 'users', function (req, res) {
    console.log('GET ' + URI_BASE + 'users');
    console.log("req.query", req.query);
    let cntQuery = Object.keys(req.query).length;
    (cntQuery > 0) ? res.send(resolveQueryString(req.query)) : res.send(userFile);
});

//Método para obtener el recurso User por su Id
app.get(URI_BASE + 'users/:id', function (req, res) {
    console.log('GET ' + URI_BASE + 'user/{id}');
    let index = req.params.id;
    let item = userFile[index - 1];
    (item) ? res.send(item) : res.send({ "mensaje": "Recurso no encontrado" });
});

//Método para registrar un recurso User, los datos a registrar deben estar en el body
app.post(URI_BASE + 'users', function (req, res) {
    console.log('POST ' + URI_BASE + 'user');
    totalUsers = userFile.length;
    let cntBody = Object.keys(req.body).length;
    if (cntBody > 0) {
        let newUser = {
            userId: totalUsers + 1,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            password: req.body.password
        }
        console.log("newUser", JSON.stringify(newUser));
        userFile.push(newUser);
        res.status(201).send({ "mensaje": "Recurso creado satisfactoriamente", "newItem": newUser });
    }
    else {
        res.status(204).send({ "mensaje": "No se se encontró cuerpo de Payload" });
    }
});

//Método para actualizar un recurso User, los datos a actualizar deben estar en el body y el id se debe encontrar en el parametro
app.put(URI_BASE + 'users/:id',
    function (req, res) {
        console.log('PUT ' + URI_BASE + 'user');
        let cntBody = Object.keys(req.body).length;
        if (cntBody > 0) {
            let idUser = req.params.id;
            let index = getIndexUser(idUser);
            console.log("index", index);
            if (index >= 0) {
                userFile[index].first_name = req.body.first_name;
                userFile[index].last_name = req.body.last_name;
                userFile[index].email = req.body.email;
                userFile[index].password = req.body.password;
                res.status(201).send({
                    "mensaje": "Usuario actualizado con exito.",
                    "Array": userFile[index]
                });
                console.log(userFile[index]);
            }
            else {
                res.status(204).send({ "mensaje": "Recurso no encontrado" });
            }
        }
        else {
            res.status(204).send({ "mensaje": "No existe Payload" });
        }
});

//Método para eliminar un recurso User por su id
app.delete(URI_BASE + 'users/:id',
    function (req, res) {
        let cntParams = Object.keys(req.params).length;
        if (cntParams > 0) {
            let idUser = req.params.id;
            let index = getIndexUser(idUser);
            if (index >= 0) {
                userFile.splice(index, 1);
                res.status(202).send({
                    "mensaje": "Usuario eliminado con exito."
                });
            } else {
                res.status(204).send({ "mensaje": "Recurso no encontrado" });
            }
        }
        else {
            res.status(204).send({ "mensaje": "No existe parametro" });
        }
});

//Método para registrar el login de un usuario y registrarlo en el archivo
app.post(URI_BASE + 'login', function (req, res) {
    console.log('POST ' + URI_BASE + 'login');
    let cntBody = Object.keys(req.body).length;
    if (cntBody > 0) {
        let userLogin = { user: req.body.email, passw: req.body.password };
        for (user of userFile) {
            if (user.email == userLogin.user) {
                if (user.password == userLogin.passw) {
                    console.log("Se encontró el usuario", user);
                    user.logged = true;
                    writeUserDataFile(userFile);
                    res.status(200).send({ "mensaje": "Login correcto", "idUsusario": user.idUser });
                    return;
                }
            }
        }
        console.log('Usuario incorrecto, favor verificar las credenciales', userLogin);
        res.status(200).send({ "mensaje": "Login incorrecto" });
    } else {
        res.status(204).send({ "mensaje": "No se se encontró cuerpo de Payload" });
    }
});

//Método para registrar el login de un usuario y registrarlo en el archivo
app.post(URI_BASE + 'logout', function (req, res) {
    console.log('POST ' + URI_BASE + 'logout');
    let cntBody = Object.keys(req.body).length;
    if (cntBody > 0) {
        let userLogin = { user: req.body.email, passw: req.body.password };
        for (user of userFile) {
            if (user.email == userLogin.user) {
                if (user.logged == true) {
                    user.logged = false;
                    writeUserDataFile(userFile);
                    res.status(200).send({ "mensaje": "Se realizó el logout correctamente" });
                    return;
                } else {
                    res.status(200).send({ "mensaje": "El usuario " + user.email + " no se encuentra logeado" });
                    return;
                }
            }
        }
        console.log('Usuario incorrecto, favor verificar las credenciales', userLogin);
        res.status(200).send({ "mensaje": "Usuario incorrecto, favor verificar el valor del usuario" });
    } else {
        res.status(204).send({ "mensaje": "No se se encontró cuerpo de Payload" });
    }
});

function getIndexUser(idUser) {
    let returnId = -1;
    for (var i = 0; i < userFile.length; i++) {
        if (userFile[i].userId == idUser) {
            console.log("encontro el valor", idUser);
            returnId = i;
        }
    }
    return returnId;
}

function writeUserDataFile(data) {
    let fs = require('fs');
    fs.writeFile("./user.json", JSON.stringify(data), "utf8", function (err) {
        if (err) {
            console.log("Se produjo un error al guardar el archivo", err);
        } else {
            console.log("Se guardo el archivo correctamente");
        }
    });
}

function resolveQueryString(query) {
    let users;
    if (query.logged != undefined) {
        users = obtainUsersByKeyValue("logged", query.logged);
    } 
    if (query.limit != undefined) {
        users = users.splice(0, query.limit);
    }
    return users;
}

function obtainUsersByKeyValue(key, value) {
    let arrayUsers = [];
    for (user of userFile) {      
        if (user[key] != undefined && (user[key]+"") == value) {
            arrayUsers.push(user);
            console.log("encontrado el logged", user);
        }
    }
    return arrayUsers;
}

app.listen(PORT, function () {
    console.log('APITechU escuchando en el puesto 3000...XD');
});